package amazonShop.Pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import io.qameta.allure.Step;

public class CheckoutForPaymentOptionPage extends BasePage  {

	public CheckoutForPaymentOptionPage(WebDriver driver) {
		super(driver);
		// TODO Auto-generated constructor stub
	}
	//*********Web Elements*********
	By CartPageBtn = By.cssSelector("#nav-cart > span:nth-child(2)");
	By ProceedToCheckout = By.cssSelector("#sc-buy-box-ptc-button > span:nth-child(1) > input:nth-child(1)");
	
	//*********Page Methods*********
	@Step ("Navigate Your Cart Page")
	public CheckoutForPaymentOptionPage Navigate_to_your_Cart_Page () {
	
		click(CartPageBtn);
		
		return this;
	}
	@Step ("Proceed to Checkout")
	public CheckoutForPaymentOptionPage Proceed_to_Checkout () {
	
		click(ProceedToCheckout);
		
		return this;
	}
}
