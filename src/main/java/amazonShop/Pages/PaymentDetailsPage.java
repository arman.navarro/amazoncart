package amazonShop.Pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import io.qameta.allure.Step;

public class PaymentDetailsPage extends BasePage {

	public PaymentDetailsPage(WebDriver driver) {
		super(driver);
		// TODO Auto-generated constructor stub
	}
	
	//*********Web Elements*********
	By AddressL1 = By.id("enterAddressAddressLine1");
	By AddressL2 = By.id("enterAddressAddressLine2");
	By City = By.id("enterAddressCity");
	By State = By.id("enterAddressStateorRegion");
	By Zip = By.id("enterAddressPostalCode");
	By GateCode = By.id("GateCode");
	By Continue = By.cssSelector(".a-button-text");
	
	//*********Page Methods*********
	
	@Step ("Fill In Checkout Details Information Address Line 1: {0}, Address Line 2: {1}, City: {2}, State: {3}"
    		+ " Zip: {4}, Gate Code: {5}, method: {method} step... ")
    public PaymentDetailsPage Fill_In_Payment_Billing_Details (
    		String Address1, String Address2,
    		String City1, String State1,
    		String Zip1, String GateCode1
    		) {
    	
    	//Enter Address Line 1
    	writeText(AddressL1, Address1);
    	
    	//Enter Address Line 2
    	writeText(AddressL2, Address2);
    	
    	//Enter City
    	writeText(City, City1);
    	
    	//Enter State
    	writeText(State, State1);
    	
    	//Enter Zip Code
    	writeText(Zip, Zip1);
    	
    	//Enter Gat Code
    	writeText(GateCode, GateCode1);
    	
    	
    	return this;	
    }
	
	@Step ("Submitting Payment Billing Information")
	public PaymentDetailsPage Continue () {
	
		click(Continue);
		
		return this;
	}
}
