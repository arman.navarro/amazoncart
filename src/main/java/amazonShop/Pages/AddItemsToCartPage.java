package amazonShop.Pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import io.qameta.allure.Step;

public class AddItemsToCartPage extends BasePage {
	
	public AddItemsToCartPage(WebDriver driver) {
		super(driver);
		// TODO Auto-generated constructor stub
	}
	//*********Web Elements*********
	By AddtoCart = By.cssSelector("div.stackToBottom:nth-child(9) > div:nth-child(1) > span:nth-child(1)");
	
	//*********Page Methods*********
	@Step ("Add at least 1 item to Cart")
	public AddItemsToCartPage Add_to_Cart_the_First_Item_from_the_Page () {
		
		click(AddtoCart);
		
		return this;
	}
}
