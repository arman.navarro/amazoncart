package amazonShop.Pages;

import io.qameta.allure.Step;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class HomePage extends BasePage {
	//*********Constructor*********
    public HomePage(WebDriver driver) {
		super(driver);
		// TODO Auto-generated constructor stub
    }

    //*********Page Variables*********
    String amazonHomeURL = "https://www.amazon.com/";
   
    
    //*********Web Elements*********
    By signInBtn = By.cssSelector("#nav-signin-tooltip > a:nth-child(1) > span:nth-child(1)");


    //*********Page Methods*********
    //Go to DashboardPage
    @Step("Go to Home Page")
    public HomePage goToHomePage (){
        driver.get(amazonHomeURL);
        return this;
    }

    
    //Go to LoginPage
    @Step("Go to Login Page")
    public HomePage goToLoginPage (){
        click(signInBtn);
        
        return this;
        
}
}
