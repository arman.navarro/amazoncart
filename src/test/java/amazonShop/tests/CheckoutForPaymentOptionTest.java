package amazonShop.tests;

import org.testng.IHookable;
import org.testng.annotations.Test;
import amazonShop.Pages.AddItemsToCartPage;
import amazonShop.Pages.CheckoutForPaymentOptionPage;
import amazonShop.Pages.HomePage;
import amazonShop.Pages.LoginPage;
import amazonShop.Pages.SelectionOfCartPage;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Story;

public class CheckoutForPaymentOptionTest extends BaseTest implements IHookable {
	@Severity(SeverityLevel.BLOCKER)
    @Description("Navigate to Login Page from Home Page")
    @Story("Login Page")
	@Test (priority = 0)
    public void Navigate_to_Login_Page_from_Home_Page () {

        //*************PAGE INSTANTIATIONS*************
        HomePage HomePage = new HomePage(driver);

        //*************PAGE METHODS********************
        HomePage.goToHomePage()
        		.goToLoginPage();
        
    }
 	
	@Severity(SeverityLevel.BLOCKER)
    @Description("Enter Email and Password to Login Page")
    @Story("Login Page")
	@Test (priority = 1)
    public void Enter_Email_and_Password_to_Login_Page () {

        //*************PAGE INSTANTIATIONS*************
		LoginPage LoginPage =  new LoginPage(driver);

        //*************PAGE METHODS********************
		LoginPage.enterEmail("armandonavarro5522@gmail.com")
                 .enterPassword("password")
                 .loginBtn();
    }
	
	@Severity(SeverityLevel.BLOCKER)
    @Description("Navigate to Selection of Cart")
    @Story("Selection of Cart Page")
	@Test (priority = 2)
    public void Navigate_to_Cart_Selection () {
		//*************PAGE INSTANTIATIONS*************
		SelectionOfCartPage SelectionOfCartPage =  new SelectionOfCartPage(driver);
		
		//*************PAGE METHODS********************
		SelectionOfCartPage.Navigate_to_Todays_Deal_Page();
		
	}
	
	@Severity(SeverityLevel.BLOCKER)
    @Description("Add the first Item to Cart")
    @Story("Selection of Cart Page")
	@Test (priority = 3)
    public void Add_Items_to_Cart () {
		//*************PAGE INSTANTIATIONS*************
		AddItemsToCartPage AddItemsToCartPage = new AddItemsToCartPage(driver);
		
		//*************PAGE METHODS********************
		AddItemsToCartPage.Add_to_Cart_the_First_Item_from_the_Page();
		
	}
	
	@Severity(SeverityLevel.BLOCKER)
    @Description("Navigate to My Cart")
    @Story("Checkout for Payment Options")
	@Test (priority = 4)
    public void Navigate_to_My_Cart_Page () {
		//*************PAGE INSTANTIATIONS*************
		CheckoutForPaymentOptionPage CheckoutForPaymentOptionPage = new CheckoutForPaymentOptionPage(driver);
		
		//*************PAGE METHODS********************
		CheckoutForPaymentOptionPage.Navigate_to_your_Cart_Page();
		
	}

	@Severity(SeverityLevel.BLOCKER)
    @Description("Proceed to Checkout")
    @Story("Checkout for Payment Options")
	@Test (priority = 5)
    public void Proceed_to_Checkout () {
		//*************PAGE INSTANTIATIONS*************
		CheckoutForPaymentOptionPage CheckoutForPaymentOptionPage = new CheckoutForPaymentOptionPage(driver);
		
		//*************PAGE METHODS********************
		CheckoutForPaymentOptionPage.Proceed_to_Checkout();
		
	}
	
	//Assumming User is a New Amazon Shopper, He needs to re-enter the Account Password for the First CheckOut Payment
	
	@Severity(SeverityLevel.BLOCKER)
    @Description("Enter Password to Continue")
    @Story("Proceed to Payment Options Password Verification")
	@Test (priority = 6)
    public void Enter_Account_Password_for_Verification () {

        //*************PAGE INSTANTIATIONS*************
		LoginPage LoginPage =  new LoginPage(driver);

        //*************PAGE METHODS********************
		LoginPage.enterPassword("password")
                 .loginBtn();
    }
}
