package amazonShop.tests;

import org.testng.annotations.Test;
import io.qameta.allure.Description;
import io.qameta.allure.Epic;
import io.qameta.allure.Feature;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Story;
import amazonShop.Pages.*;
import org.testng.IHookable;

@Epic("Selection of Cart")
@Feature("Selection of Cart View")
public class SelectionOfCartTest extends BaseTest implements IHookable {
	
	@Severity(SeverityLevel.BLOCKER)
    @Description("Navigate to Login Page from Home Page")
    @Story("Login Page")
	@Test (priority = 0)
    public void Navigate_to_Login_Page_from_Home_Page () {

        //*************PAGE INSTANTIATIONS*************
        HomePage HomePage = new HomePage(driver);

        //*************PAGE METHODS********************
        HomePage.goToHomePage()
        		.goToLoginPage();
        
    }
 	
	@Severity(SeverityLevel.BLOCKER)
    @Description("Enter Email and Password to Login Page")
    @Story("Login Page")
	@Test (priority = 1)
    public void Enter_Email_and_Password_to_Login_Page () {

        //*************PAGE INSTANTIATIONS*************
		LoginPage LoginPage =  new LoginPage(driver);

        //*************PAGE METHODS********************
		LoginPage.enterEmail("armandonavarro5522@gmail.com")
                 .enterPassword("password")
                 .loginBtn();
    }
	
	@Severity(SeverityLevel.BLOCKER)
    @Description("Navigate to Selection of Cart")
    @Story("Selection of Cart Page")
	@Test (priority = 2)
    public void Navigate_to_Cart_Selection () {
		//*************PAGE INSTANTIATIONS*************
		SelectionOfCartPage SelectionOfCartPage =  new SelectionOfCartPage(driver);
		
		//*************PAGE METHODS********************
		SelectionOfCartPage.Navigate_to_Todays_Deal_Page();
		
	}
	
	
	
}
